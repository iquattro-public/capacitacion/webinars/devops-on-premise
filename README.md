# DEVOPS WEBINAR ON PREMISE - 2024

![Markdown](https://img.shields.io/badge/markdown-%23000000.svg?style=for-the-badge&logo=markdown&logoColor=white)
![Shell Script](https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white)
![GitLab CI](https://img.shields.io/badge/gitlab%20CI-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)
![Ansible](https://img.shields.io/badge/ansible-%231A1918.svg?style=for-the-badge&logo=ansible&logoColor=white)
![](https://img.shields.io/badge/version-0,1-red)

![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white)
![Vagrant](https://img.shields.io/badge/vagrant-%231563FF.svg?style=for-the-badge&logo=vagrant&logoColor=white)
![Terraform](https://img.shields.io/badge/terraform-%235835CC.svg?style=for-the-badge&logo=terraform&logoColor=white)
![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)

![Fedora](https://img.shields.io/badge/Fedora-294172?style=for-the-badge&logo=fedora&logoColor=white)
![Debian](https://img.shields.io/badge/Debian-D70A53?style=for-the-badge&logo=debian&logoColor=white)
![Ubuntu](https://img.shields.io/badge/Ubuntu-E95420?style=for-the-badge&logo=ubuntu&logoColor=white)
![DigitalOcean](https://img.shields.io/badge/DigitalOcean-%230167ff.svg?style=for-the-badge&logo=digitalOcean&logoColor=white)

![YAML](https://img.shields.io/badge/yaml-%23ffffff.svg?style=for-the-badge&logo=yaml&logoColor=151515)
![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)
![Visual Studio Code](https://img.shields.io/badge/Visual%20Studio%20Code-0078d7.svg?style=for-the-badge&logo=visual-studio-code&logoColor=white)
![Vim](https://img.shields.io/badge/VIM-%2311AB00.svg?style=for-the-badge&logo=vim&logoColor=white)

![Operating System](https://img.shields.io/badge/platform-linux-success.svg)

## Ejemplos Básicos

- **vagrant-basic:** Esta carpeta contiene una ejemplo inicial básico de la creación de una máquina virtual para VirtualBox, el ejemplo ejecuta la instalación de un servidor Web Nginx sin configuraciones iniciales, los comandos para inicializar el demo son:

```bash
$ git clone git@gitlab.com:iquattro-public/capacitacion/webinars/devops-on-premise.git
$ cd vagrant-basic
$ vagrant up
$ vagrant ssh
$ vagrant destroy
```

- **ansible:** Instalación de NodeJS, Yarn y Tomcat9, manejo de clases SSH, configuraciones simples.

Se debe agregar una clave SSH pública para el acceso remoto al servidor, esta operación se puede automatizar pero para el ejemplo se muestra el proceso manual (no se detalla el ejemplo de generado de claves SSH).

Para la configuración de ansible es necesario obtener la IP de la máquina virtual.

```bash
$ cat ~/.ssh/devops-ssh.pub
$ cd ansible
$ ansible-playbook base.yml -i inventory.yml --ask-become-pass
$ ssh -i ~/.ssh/devops-ssh vagrant@[IP_VM]
```

- **vagrant-web:** Este un ejemplo de la creación de una máquina virtual para VirtualBox, el ejemplo ejecuta la instalación de un servidor Web Nginx con PHP, genera un archivo **index.php** los comandos para inicializar el demo son:

```bash
$ git clone git@gitlab.com:iquattro-public/capacitacion/webinars/devops-on-premise.git
$ cd vagrant-web
$ vagrant up
$ vagrant ssh
$ vagrant destroy
```

## Referencias

- [Gitlab SSH](https://docs.gitlab.com/ee/user/ssh.html)
- [Markdown Badges](https://github.com/Ileriayo/markdown-badges)
- [Digitalocean Docs](https://docs.digitalocean.com/products/)