#!/bin/bash

## Configuración Servidor Web
sudo cp -f nginx.conf /etc/nginx/nginx.conf
sudo cp -f default /etc/nginx/sites-available/default
sudo mkdir /var/www/html/web-demo
sudo systemctl restart nginx.service

echo "<?php phpinfo(); ?>" | sudo tee /var/www/html/web-demo/index.php
